<?php declare(strict_types=1);

include './vendor/autoload.php';

use Laminas\Diactoros\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Controller\SomeController;
$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$router = new League\Route\Router;
$responseFactory = new \Laminas\Diactoros\ResponseFactory();

$router->map('GET', '/departements', 'Controller\SomeController::someMethod');


//$router->map('GET', '/', function (ServerRequestInterface $request): ResponseInterface {

  //  $response = new Laminas\Diactoros\Response;
  /*  $response = file_get_contents('Vu/vu.php', true);
    echo $response;

});*/


$response = $router->dispatch($request);

// send the response to the browser
(new Laminas\HttpHandlerRunner\Emitter\SapiEmitter)->emit($response);