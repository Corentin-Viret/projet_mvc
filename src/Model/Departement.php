<?php declare(strict_types=1);

	namespace Model;

class Departement
	{
					private $name;

					public function __construct($name)
					{
									$this->name = $name;
					}

					public function getName()
					{
									return $this->name;
					}

				public function setName($name)
					{
									$this->name = $name;
					}
	}