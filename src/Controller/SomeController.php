<?php declare(strict_types=1);

namespace Controller;

use Symfony\Component\HttpClient\HttpClient;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Laminas\Diactoros\Response;
use Doctrine\Common\Collections\ArrayCollection;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Model\Departement;

class SomeController
{
				private $departments;

				public function __construct()
				{
								$this->departments = new ArrayCollection();
				}

				private function getDepartmentByName($name)
				{
									foreach($this->departments as $department) {
														if($department->getName() === $name) {
																			return $department;
														}
									}
									return null;
				}
    /**
     * Controller.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
				public function someMethod(ServerRequestInterface $request): ResponseInterface
				{

								// on récupère le JSON
								$client = HttpClient::create();
								$response = $client->request('GET', 'https://www.data.gouv.fr/fr/datasets/r/b4637758-1184-497c-b8ea-a24d456163d0');
							
							 $datas = $response->toArray();

								// on parcours le son pour créer la liste des départments
								foreach($datas as $data) {
													$name = $data['fields']['departement'];
													$department = $this->getDepartmentByName($name);
													if($department == null) {
																		$department = new Departement($name);
																		$this->departments->add($department);
													}
								}
//dump($this->departments);
								// on génère la vue à partir d'un template twig
$loader = new FilesystemLoader(__DIR__ . '/../../templates');
$twig = new Environment($loader, [
    'cache' => __DIR__ . '/../../cache',
]);

$content = $twig->render('some.html.twig', ['departements' => $this->departments]);
dump($content);
								// On retourne la réponse http
								$response = new Response;
								$response->getBody()->write($content);
								return $response;

				}
}